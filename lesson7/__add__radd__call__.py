#методы для разных порядков слледования операндов(т.е. левосторонее словдение __add__ и когда экземпляр
#класс находится справа __radd__)
class Commuter:
    def __init__(self, val):
        self.val = val
    def __add__(self, other):
        print('add', self.val, other)
        return self.val + other
    def __radd__(self, other):
        print('radd', self.val, other)
        return other + self.val
x = Commuter(88)
y = Commuter(99)
x+1
1+x
z=x+y

# реализация вызова экземпляра

class Callee:
    def __call__(self, *pargs, **kargs): # Реализует вызов экземпляра
        print('Called:', pargs, kargs) # Принимает любые аргументы

C = Callee()
'''Выражаясь более формальным языком, метод __call__ поддерживает все схе-
мы передачи аргументов – все, что передается экземпляру,
передается этому методу наряду с обычным аргументом self, в ко-
тором передается сам экземпляр'''
C('a', 2, 3, x = 36)
C(*[1, 2], **dict(c=3, d=4)) # распаковывание


