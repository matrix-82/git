__author__ = 'Seryga'

def div():
    print("-"*40)

class Person:
    def __init__(self,name,job=None,wage=0):
        self.name = name
        self.job = job
        self.pay = wage
    def lastName(self):
        return self.name.split()[-1]
    def giveRaise(self,percent):
        self.pay =int(self.pay*(1+percent/100))
    def __str__(self):
        return '[Person: %s,%s,%i]' %(self.name,self.job,self.pay)

first = Person("Igor Alexseev","gamedev",20000)
print(first.lastName())
second = Person("Alexandra Kotova","QA",50000)
third = Person("Simon Ivanov", "Frontend",13000)
it = Person("Johnny")

print(first.name,first.job,first.pay)
print(it.pay, it.job, it.name)
print(first.name,first.job,first.pay+2000)

div()                                 #Методы реализации(14 __str__)
print(second.pay)
second.giveRaise(25)
print(second.pay)

div()
print('*'*40)
#'перегрузка операторов'
print(first)
div()

class Manager(Person):
    def giveRaise(self,percent,bonus=1):
        Person.giveRaise(self,percent+bonus)
    def __init__(self,name,pay):              # переопределенный конструктор
        Person.__init__(self,name,'mngr',pay)

class Department:
    def __init__(self, *args):
        self.members = list(args)
    def addMember(self, person):
        self.members.append(person)
    def giveRaises(self, percent):
        for person in self.members:
            person.giveRaise(percent)
    def showAll(self):
        for person in self.members:
            print(person)

if __name__ == '__main__':
    first = Person('Igor Alexseev')
    second = Person('Alexandra Kotova', 'QA', 15000)
    print(first)
    print(second)
    print(first.lastName(), second.lastName())
    second.giveRaise(.10)
    print(second)
    mngr = Manager('Vitalii Pogoreloff',30000)
    mngr.giveRaise(.10)
    print(mngr.lastName())
    print(mngr)
    div()                       #Полиморфизм
    print('--All three--')
    for object in [first, second, mngr]:
        object.giveRaise(.10)
        print(object)

    person = Person('vacancy')
    person.name = 'Evgeniy Ruban'
    person.job = 'engeneer'
    person.pay = 40000
    print('IN Department')
    development = Department(first, second) # Встраивание объектов в составной объект
    development.addMember(mngr)
    development.giveRaises(10) # Вызов метода giveRaise вложенных объектов
    development.showAll() # Вызов метода __str__ вложенных объектов
    l=[]
    for it in second.__dict__:
        l.append([it, '->',second.__dict__.get(it)])
    print(l)
    ls=[]
    for key in person.__dict__:
        ls.append([key,getattr(person,key)])
    print(ls)
    print ('second',dir(second)) # dir получаем атрибуты и от унаследованных классов+методы типа класса



