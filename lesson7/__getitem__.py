class Number:
    def __init__(self, start): # Вызов Number(start)
        self.data = start
    def __sub__(self, other): # Выражение: экземпляр - other
        return Number(self.data - other) # Результат – новый экземпляр
X = Number(5) # Number.__init__(X, 5)
Y = X - 2 # Number.__sub__(X, 2)
z = X.__sub__(2)# эквивалентно
print('working class Number:',Y.data,z.data) # Y - новый экземпляр класса Number

class Indexer:
    def __getitem__(self, index):
        return index ** 2
X = Indexer()
print('Working class Indexer:',X[2])                # Выражение X[i] вызывает X.__getitem__(i)
for i in range(5):
    print(X[i], end=' ') # Вызывает __getitem__(X, i) в каждой итерации
print('\n','-'*20)

class Indexer1:
    def __init__(self,data):
        self.data = data
    def __getitem__(self, index): # Вызывается при индексировании или
        print('getitem:', index) # извлечении среза
        return self.data[index] # Выполняет индексирование
X = Indexer1([5,6,7,8,9])
print('Working class Indexer1:',X[2],X[1],X[-2],X[1:4]) # При индексировании __getitem__

class stepper:
    def __getitem__(self, i):
        return self.data[i]
X = stepper() # X - это экземпляр класса stepper
X.data = "Spam"
print("Working class stepper:",X[0]) # Индексирование, вызывается __getitem__
for item in X: # Циклы for вызывают __getitem__
    print(item, end=' ')
print('\n','p' in X)

print('str.upper(str([c for c in X]=',str.upper(str([c for c in X])))
print('list(map(str.upper, X))=',list(map(str.upper, X)))
print('str.upper(X.data)=',str.upper(X.data))

(a, b, c, d,) = X
print('a = '+a+', ''b = '+b)
