class Squares:
    def __init__(self, start, stop): # Сохранить состояние при создании
        self.value = start - 1#0
        self.stop = stop      #5
    def __iter__(self): # Возвращает итератор в iter()
        return self
    def __next__(self): # Возвращает квадрат в каждой итерации
        if self.value == self.stop: # Также вызывается функцией next
            raise StopIteration
        self.value += 1  #1
        return self.value ** 2
for i in Squares(1, 5): # for вызывает iter(), который вызывает __iter__()
    print(i, end=' ') # на каждой итерации вызывается __next__()

# итерации вручную

X = Squares(1,5)
I = iter(X)
print('\n')
print(next(I))
print(next(I))
print(next(I))
print(next(I))
print(next(I))
#print(next(I)) # этот принт вызовет встроенное исключение - StopIteration

# Реализация с помощью функции-итератора
def gsquares(start, stop):
    for i in range(start, stop+1):
        yield i**2
#print (gsquares(1,5)) - ничего не дает
for i in gsquares(1,5):
    print(i, end = ' ')

# еще проще:
print([x**2 for x in range(1,6)])

S = '123'
for x in S:
    for y in S:
        print(x + y, end=' ')

