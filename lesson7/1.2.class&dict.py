__author__ = 'Seryga'
q = {}
q['name'] = 'Tranduil'
q['age'] = 200
q["job"] = "Elvenking of Mirkwood"
print(q)

print('-'*40)

print(q.keys())
print(q.values())

print('-'*40)

class Q:
    pass
pers1 = Q()
pers1.name = 'Oakenshield'
pers1.age = 195
pers1.job = 'King under the Mountain'
print(pers1.name,pers1.age,pers1.job)

print('-'*40)

class Q1:
    def __init__(self,name,age,job):
        self.name = name
        self.job = job
        self.age = age
    def info(self):
        return print((self.name,self.age,self.job))
pers1 = Q1('Oakenshield',195,'King under the Mountain')
pers2 = Q1('Tranduil',200,"Elvenking of Mirkwood")
pers1.info()
pers2.info()