class MixedNames: # Определение класса
    data = 'spam' # Присваивание атрибуту класса
    def __init__(self, value): # Присваивание имени метода
        self.data = value # Присваивание атрибуту экземпляра
    def display(self):
        print(self.data, MixedNames.data) #

class NextClass: # Определение класса
    def printer(self, text): # Определение метода
        self.message = text # Изменение экземпляра
        print(self.message) # Обращение к экземпляру

class Super:
    def method(self):
        print('in Super.method')
class Sub(Super):
    def method(self): # Переопределить метод
        print('starting Sub.method')# Дополнительное действие
        Super.method(self) # Выполнить действие по умолчанию
        print('ending Sub.method')

x = MixedNames(1)
y = MixedNames(2)
x.display()
y.display()

z = NextClass()
z.printer('dzjango') #вызов метода через экземпляр
NextClass.printer(z,'retro porno')# прямой вызов метода класса
print(x.data, y.data)

t = Super()
t.method()
s = Sub()
s.method()
Super.method(s)
Sub.method(t)