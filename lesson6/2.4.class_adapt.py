__author__ = 'Seryga'
class FirstClass:                           # Определяет объект класса
    def setdata(self, value):               # Определяет метод класса
        self.data = value                   # self – это экземпляр
    def display(self):
        print('firstclass',self.data)


class SecondClass(FirstClass):              # Наследует setdata
    def display(self):                      # Изменяет display
        print('Current value = "%s"'
              % self.data)


class ThirdClass(SecondClass):              # Наследует SecondClass
    def __init__(self, value):              # Вызывается из ThirdClass(value)
        self.data = value
    def __add__(self, other):               # Для выражения “self + other”
        return ThirdClass(self.data + other)
    def __str__(self):                      # Вызывается из print(self), str()
        return '[ThirdClass: %s]' % self.data
    def mul(self, other):                   # Изменяет сам объект: обычный метод
        self.data *= other
a = ThirdClass('abc')                  # Вызывается новый метод __init__
a.display()                                 # Унаследованный метод
print(a)                                    # __str__: возвращает строку
b = a + ' xyz'                                # Новый __add__: создается новый экземпляр
b.display()
print(b)                                    # __str__: возвращает строку
a.mul(3)                                    # mul: изменяется сам экземпляр
print('a=',a)


'''
Метод __init__ вызывается, когда создается новый объект экземпляра (ар-
гумент self представляет новый объект ThirdClass).1
Метод __add__ вызывается, когда экземпляр ThirdClass участвует в опера-
ции +.
Метод __str__ вызывается при выводе объекта (точнее, когда он преобразу-
ется в строку для вывода вызовом встроенной функции str или ее эквива-
лентом внутри интерпретатора).'''