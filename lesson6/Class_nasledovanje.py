class FirstClass: # Определяет объект класса
 def setdata(self, value): # Определяет метод класса
     self.data = value # self – это экземпляр
 def display(self):
     print('значение равно:',self.data)

class SecondClass(FirstClass): # Наследует setdata
    def display(self): # Изменяет display, переопределяет такой же метод класса FirsClass
        print('Реальное значение равно "%s"' % self.data)

class ThirdClass(SecondClass): # Наследует SecondClass
    def __init__(self, value): # Вызывается из ThirdClass(value)
        self.data = value
    def __add__(self, other): # Для выражения “self + other”
        return ThirdClass(self.data + other)
    def __str__(self): # Вызывается из print(self), str()
        return '[ThirdClass: %s]' % self.data
    def mul(self, other): # Изменяет сам объект: обычный метод
        self.data *= other

def upperName(self):
    return self.data.upper()

x = FirstClass()
y = FirstClass()
x.setdata('хрен')#FirstClass.setdata(x,'хрен')
y.data = 'вам'
z = SecondClass()
z.setdata(x.data +' '+ y.data)
#z.display()


a = ThirdClass('Виктор') # Вызывается новый метод __init__
#a.display() # Унаследованный метод
#print(a) # __str__: возвращает строку
b = a + ' комон' # Новый __add__: создается новый экземпляр
#b.display()
#print(b) # __str__: возвращает строку
b.mul(3) # mul: изменяется сам экземпляр
#print(b)
#print(z) # print доступен только для обьектов третьего класса
f = FirstClass()
#print(ThirdClass.__bases__,'->',SecondClass.__bases__,'->',FirstClass.__bases__)#ветка наследования
#print(upperName(z))