__author__ = 'Seryga'

class Closer:
    '''Менеджер контекста для автоматического закрытия объекта вызовом метода close
    в with-выражении.'''

    def __init__(self, obj):
        self.obj = obj

    def __enter__(self):
        return self.obj # привязка к активному объекту with-блока

    def __exit__(self, exception_type, exception_val, trace):
        try:
           self.obj.close()
        except AttributeError: # у объекта нет метода close
           print ('Not closable.')
           return True