__author__ = 'Seryga'

class FuncHolder:
    def func(self):
        pass
fh = FuncHolder()

print(FuncHolder.func,"\n")     # <function func at 0x8f806ac>
print(FuncHolder.__dict__,"\n") # {...'func': <function func at 0x8f806ac>...}
print(fh.func)             # <bound method FuncHolder.func of <__main__.FuncHolder object at 0x900f08c>>

print(FuncHolder.func.__class__.__get__)


