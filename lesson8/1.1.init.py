__author__ = 'Seryga'
class Number:
    def __init__(self, start):                  # Вызов Number(start)
        self.data = start
    def __sub__(self, other):                   # Выражение: экземпляр - other
        return Number(self.data - other)        # Результат – новый экземпляр
