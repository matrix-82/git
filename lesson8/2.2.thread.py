__author__ = 'Seryga'
from threading import Thread
from time import sleep

class MyThread(Thread):
    def __init__(self):
        Thread.__init__(self)
    def run(self):
        for i in range(20):
            sleep(0.5)
    def hello(self):
        print("hello")
    def Gb(self):
        print("finish")


c = MyThread()
b = MyThread()
c.start()
c.hello()
b.start()
b.hello()
c.join() # дождаться завершения потока в основном потоке
b.Gb()
b.join()
b.Gb()