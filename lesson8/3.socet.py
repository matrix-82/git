import socket

sock = socket.socket()
sock.bind(('', 9090))# '' пустая строка еквив localhost
sock.listen(1) # раз в сек проверка,прослушка порта
conn, addr = sock.accept()

print('connected:', addr)

while True:
    data = conn.recv(1024)
    if not data:
        break
    conn.send(data.upper())

conn.close()