__author__ = 'Seryga'

# Извлечение среза с использованием синтаксиса срезов

L = [5, 6, 7, 8, 9]
print('#1 ',L[2:4])
print('#2 ',L[1:])
print('#3 ',L[:-1])
print('#4 ',L[::2])

# Извлечение среза с помощью объекта среза

print('#5 ',L[slice(2, 4)])
print('#6 ',L[slice(1, None)])
print('#7 ',L[slice(None, -1)])
print('#8 ',L[slice(None, None, 2)])

class Indexer():
    data = [5, 6, 7, 8, 9]
    def __getitem__(self,index):           # Вызывается при индексировании или
        print('getitem:', index)            # извлечении среза
        return self.data[index]             # Выполняет индексирование

X = Indexer()
print(X[0])                                    # При индексировании __getitem__
print(X[1])
print(X[-1])
