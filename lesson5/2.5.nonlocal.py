__author__ = 'Seryga'
from myfunc import separator
def tester(start):
    state = start # В каждом вызове сохраняется свое значение state
    def nested(label="number"):
        nonlocal state # Объект state находится
        print(label, state) # в объемлющей области видимости
        state += 1 # Изменит значение переменной, объявленной как nonlocal
    return nested


F = tester(1)
F() # Будет увеличивать значение state при каждом вызове
F()
F()
separator()
G = tester(-1)
G()
G()
G()
separator()

