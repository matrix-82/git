__author__ = 'Seryga'
import myfunc
def func(a, b, c=2): # c - необязательный аргумент
    return a + b + c
print(func(1, 2))
myfunc.separator()
print(func(1, 2, 3))
myfunc.separator()
print(func(a=3,b=6))
#print(func(a=3, c=6)) # a = 3, c = 6, b не определен
