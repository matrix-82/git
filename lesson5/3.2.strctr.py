__author__ = 'Seryga'
import math
def sumtree(L):
    tot = 0
    for x in L: # Обход элементов одного уровня
        if not isinstance(x, list):
            tot += x # Числа суммируются непосредственно
        else:
            tot += sumtree(x) # Списки обрабатываются рекурсивными вызовами
    return tot
L = [1, [2, [3, 4], 5], 6, [7, 8]]
print(sumtree(L))
print(sumtree([1, [2, [3, [4, [5]]]]])) # Выведет 15, справа налево
print(sumtree([[[[[1], 2], 3], 4], 5])) # Выведет 15, слева направо
print(sum([1,2,2,3,4,5,6,7]))