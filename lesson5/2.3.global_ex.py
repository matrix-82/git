__author__ = 'Seryga'
X = 99              # Имя в глобальной области видимости: не используется
def f1():
    X = 88          # Локальное имя в объемлющей функции
    def f2():
        print(X)    # Обращение к переменной во вложенной функции
    f2()
f1()                # Выведет >>>: локальная переменная в объемлющей функции


def f3():
    X = 88
    def f2():
        print(X)    # Сохраняет значение X в объемлющей области видимости
    return f2 # Возвращает f2, но не вызывает ее
action = f3()       # Создает и возвращает функцию
action()            # Вызов этой функции: выведет >>>