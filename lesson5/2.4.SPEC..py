__author__ = 'Seryga'

def home():
    x = '*'
    y = '*'
    z = ' *'
    r = '*'
    w = 'x'

    def wall(a=0):
        while a < 3:
            print(x, " "*6, x)
            a += 1
        return x

    def floor(b=0):
        while b <= 9:
            print(y, end="")
            b += 1
        return y

    def roof(a=1, b=2, c=3, d=4):
        if a == 1:
            print(3*" "+z+3*" ")
            if b == 2:
                print(2*" "+z+" "+z+2*" ")
                if c == 3:
                    print(" "+z+"   "+z+" ")
                    if d == 4:
                        print(z+5*' '+z)
        return z

    def ceiling():
        print(r*10)
        return r

    def window(s=0):
        while s < 2:
            print("*   ***  *")
            s += 1
            if s==1:
                print("*   * *  *")
        return s



    roof()
    ceiling()
    wall()
    window()
    wall()
    floor()
home()
