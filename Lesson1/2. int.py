__author__ = 'Seryga'
# неявная типизация

c = 260+2
print(c)
c = 300-140
print(c)
f = 20/3
print(f)
c = 20//3
print(c)
c = 30//4
print(c)
print('-'*40)
print(20 % 2)
print(30 % 4)
print(3**4)
print(pow(3, 4))
print(2**1000)