__author__ = 'Seryga'
N = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
col1 = [row[0] for row in N]
col2 = [row[1] for row in N]
col3 = [row[2] for row in N]
print(N)

print('_'*40)

print(col2)
print('_'*40)

print(col1)
col1.extend(col2)
print('_'*40)
print(col1)

print('='*40)
col2 = [row[1]+1 for row in N]
print("col2", col2 ," ", "col3", col3)