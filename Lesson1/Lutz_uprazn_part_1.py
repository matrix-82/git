__author__ = 'Matrix'

def fun_ord(strn):
    sm=[]
    for char in strn:
        sm.append(ord(char))
    print("ASCII code of string",strn,"is",str(sm)[1:-1]) #str(sm).strip('[]') or ', '.join(map(str, sm)

def sum_ord(str):
    z=0
    for i in str:
        z+=ord(i)
    print("sum of string",str,"is..",z)

S = 'spam'
fun_ord(S)
sum_ord(S)

for s in S:
    print(ord(s), end=' ')

print()
print(list(map(ord,S)))

#вывод элементов массива в порядке возрастания
# вариант 1
d = dict(a=1,b=2,c=3,d=4,e=5,f=6,g=7)
a=str(sorted(d.values()))
print(a.strip('[]'))

#вариант 2
a=[]
for i in d:
    a.append(d[i])
print(sorted(a))

#вариант 3
i=sorted(list(d.keys()))
for key in i:
    print(key,'-->',d[key])


import winsound
for i in range(5):
    winsound.Beep(38,300)
    print( 'hello %d'% i)

