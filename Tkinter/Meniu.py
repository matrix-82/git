from tkinter import *

root = Tk()
def colorR():
    fr.config(bg='red')
def colorG():
    fr.config(bg='green')
def colorB():
    fr.config(bg='blue')
def size500():
    fr.config(width=500,height=500)
def size700():
    fr.config(width=700,height=400)

m = Menu(root) #создается объект Меню на главном окне
root.config(menu=m) #окно конфигурируется с указанием меню для него
fm = Menu() #создается пункт меню с размещением на основном меню (m)
nfm = Menu()
m.add_cascade(label="Color",menu=fm) #пункту располагается на основном меню (m)
fr = Frame(root, width = 300,height=300, bg ='grey',bd =20)
fr.pack()
fm.add_command(label="Red",command=colorR)
fm.add_command(label="Green",command=colorG)
fm.add_command(label="Blue",command=colorB)
m.add_cascade(label="Size",menu=nfm)
nfm.add_cascade(label = "500x500",command=size500)
nfm.add_cascade(label = "700x400",command=size700)


#hm.add_command(label="About",command=about)

mainloop()