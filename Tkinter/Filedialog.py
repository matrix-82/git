from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *
import  fileinput
import random
def _exit():
    if askyesno("Exit", "Do you want to quit?"):
        root.destroy()
def _open():
    op = askopenfilename()
    for l in fileinput.input(op):
        txt.insert(END,l)
def _save():
    sa = asksaveasfilename()
    letter = txt.get(1.0,END)
    f = open(sa,'w')
    f.write(letter)
    f.close()
def _about():
    win =Toplevel(root)
    lb=Label(win,text='This is a testing version\n owner Ruban E.V.')
    lb.pack()
root = Tk()
root.title("Редактор")
m = Menu(root)
root.config(menu=m)
fm = Menu(m)
txt=Text(root)
txt.pack()
m.add_cascade(label="File",menu=fm)
fm.add_command(label="Open...",command=_open)
fm.add_command(label="Save...",command=_save)
m.add_command(label="About",command=_about)
m.add_command(label="Exit",command=_exit)

root.mainloop()

