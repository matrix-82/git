
class super:
    def hello(self):
        self.data1 = 'spam'
class sub(super):
    def hola(self):
        self.data2 = 'eggs'

X = sub()
print(X.__dict__)#пространство имен
print(X.__class__)#принадлежность к конкретному классу
print(sub.__bases__)#суперклассы(super)
print(super.__bases__)

Y = sub()
X.hello()
print(X.__dict__)
print(super.__dict__)
X.hola()
print(X.__dict__)
print(sub.__doc__)
print(super.__dict__.keys())
print(Y.__dict__)
Z = sub()
print(Y.__dict__.keys() == Z.__dict__.keys())
print(X.data1, X.__dict__['data1'])#два варианта обращения по ключу
X.data3 = 'toast'
print(X.__dict__)
X.__dict__['data3'] = 'Hongildon'
print(X.__dict__)
print(dir([X,sub,super]))