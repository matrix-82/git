# cmd --> python os_path.py .  .-current dir, or some else, ('C:\Windows')
import sys, os

def Files_on_dir(dir):
    try:
        files = os.listdir(dir)
        for file in files:
            path = os.path.join(dir,file)
            print(path)
            print os.path.abspath(path)
    except:
        print('Error')

def main():
    Files_on_dir(sys.argv[1])

if __name__ == '__main__':
  main()

