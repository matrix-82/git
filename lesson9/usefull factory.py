from __future__ import generators
import random


class Cook(object):
    sweet = ["sweet","bitter","salty"]
    flavours = ["meat","eggs","cherry","apple"]
    a = flavours.pop()
    b = sweet.pop()

quant = int(input("how many snacks?\n"))

class Bake(object):

    def process(type):
        if type == "Pie":
            return Pie()
        if type == "Cake":
            return Cake()
    process = staticmethod(process)


class Pie(Bake):
    def make(self):
        random.shuffle(Cook.flavours)
        random.shuffle(Cook.sweet)
        print("Next pie would be",Cook.b,"with",Cook.a,"flavour")
        return self
    def pack(self):
        print("packed")
        return self


class Cake(Bake):
    def make(self):
        random.shuffle(Cook.flavours)
        random.shuffle(Cook.sweet)
        print("Next Cake would be",Cook.b,"with",Cook.a,"flavour")
    def pack(self):
        print("packed")
        #return self

def preference(n):
    products = Bake.__subclasses__()
    for i in range(n):
        yield random.choice(products).__name__

queue =[Bake.process(i) for i in preference(quant)]

for snack in queue:
    snack.make()
    snack.pack()