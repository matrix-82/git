#from __future__ import generators
import random

class Shape(object):

    def factory(type):

        if type == "Circle": return Circle()
        if type == "Square": return Square()
        assert 0, "Bad shape creation: " + type
    factory = staticmethod(factory)

class Circle(Shape):
    def draw(self): print("Circle")

class Square(Shape):
    def draw(self): print("Square")



def shapeNameGen(n):
    types = Shape.__subclasses__()
    for i in range(n):
        yield random.choice(types).__name__

shapes = [ Shape.factory(i) for i in shapeNameGen(1)]

for shape in shapes:
    shape.draw()
