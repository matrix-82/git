__author__ = 'Seryga'
T = (1, 2, 3, 4) # Кортеж из 4 элементов
print(len(T))
A = T + (5, 6)
print(T[0]) # Извлечение элемента, среза и так далее
print(A)

print(A.index(5))

print(T.count(4))

# T[0] = 2

T = ('spam', 3.0, [11, 22, 33], ['hey', 2, 45])
print('-'*40)
print(T)
print(T[0])
print(T[3][1][2])

