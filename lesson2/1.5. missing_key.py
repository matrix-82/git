__author__ = 'Seryga'
D = {'a': 1, 'c': 3, 'b': 2}
D['e'] = 99  # Присваивание по новому ключу приводит к расширению словаря
print(D)
D['f']=33
print(D)

print('f' in D, end=" ")
if not 'f' in D:
    print('missing')
else:
    print('here it is', D['f'])


