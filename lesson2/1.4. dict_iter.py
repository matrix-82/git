__author__ = 'Seryga'
# Итерации и оптимизация

squares = [x ** 2 for x in [1, 2, 3, 4, 5]]
print(squares)

square = []
for x in [1, 2, 3, 4, 5]:   # Эти же операции выполняет и генератор списков,
    square. append(x ** 2)  # следуя протоколу итераций
print(square)

