__author__ = 'Seryga'
D = {'food': 'gre4a', 'quantity': 7, 'color': 'black'}
print(D)
print("-" * 40)
print(D['food'])
del D['color']
print(D)
# Мы можем обращаться к элементам этого словаря по ключам
# и изменять значения, связанные с ключами.
print("-" * 40)

D['quantity'] += 1
print(D['quantity'])

print("*" * 40)

print(D)

print("*" * 40)
A = {}
A['name'] = 'Andrey'
A['job'] = 'student'
A['age'] = 18
print(A)

print(A['job'])



