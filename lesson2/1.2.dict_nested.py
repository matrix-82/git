__author__ = 'Seryga'
rec = {'name': {'first': 'Andrey', 'last': 'Ivanov'},
'job': ['student', '1-st grade'],
'age': 20}
print( rec['name']) # 'Name' – это вложенный словарь
print( rec['name']['last']) # Обращение к элементу вложенного словаря
print( rec['job']) # 'Job' – это вложенный список
print( rec['job'][1]) # Обращение к элементу вложенного списка
print( rec['job'].append('janitor')) # Расширение списка должностей Боба (Bob)
print(rec)
rec = {rec[0]}
print('*'*40)
print(rec)