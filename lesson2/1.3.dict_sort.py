__author__ = 'Seryga'

D = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
print(D)

print('-'*40)

Ks =(list(D.keys()))
print(Ks)

Ks.sort()
print(Ks)

print(':'*40)

# цикл for

for key in Ks : print(key, '=>', D[key])  # Обход отсортированного списка ключей

print(':'*40)

for c in 'spam':
    print (c.upper(), end=' ')

print('-'*40)

# цикл while
a = 0
b = 1
while b < 1000:
    print(b, end=' ')
    a = b
    b = a + b

