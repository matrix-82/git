__author__ = 'Seryga'
from fractions import Fraction
x = Fraction(1, 3)  # Числитель, знаменатель
y = Fraction(4, 6)  # Будет упрощено до 2, 3 с помощью функции gcd
print(x)
print(y)
print('-'*40)
print(x/y)
print(y/x)
print(x+y)
print(x*y)
print(x-y)

# Рациональные числа могут создаваться из строк с представлением вещественных чисел
print('-'*40)

print(Fraction('.25'))
print(Fraction(1, 4))
print(Fraction('1.25'))
print(Fraction(5, 4))
print(Fraction('.25') + Fraction('1.25'))
print(Fraction(3, 2))