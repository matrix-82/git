__author__ = 'Seryga'
print(0.2 + 0.2 + 0.2 - 0.6)
print(0.1 + 0.1 + 0.1 - 0.3)
print(0.3 + 0.3 + 0.4 - 1.0)
print(1.5 + 0.25 + 0.25 - 2.0)
print('-'*40)
from decimal import Decimal  # использовании чисел с фиксированной точностью
c = Decimal('0.2') + Decimal('0.2') + Decimal('0.2') - Decimal('0.6')
print(c)
e = Decimal('0.1') + Decimal('0.100') + Decimal('0.10') - Decimal('0.30')
print(e)
