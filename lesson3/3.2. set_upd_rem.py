__author__ = 'Seryga'
z = set(['b', 'd', 'SPAM'])
z.update(set(['X', 'Y']))       # Объединение множеств
print(z)
z.remove('b')                   # Удалит один элемент
print(z)

print('-'*40)
for item in set('abc'): print(item * 3)
print('-'*40)
s = set([1, 2, 3])
s | set([3, 4]) # Операторы выражений требуют,
print(s)
#S | [3, 4]
s.union([3, 4])
s.intersection((1, 3, 5))
s = set([1, 3])
s.issubset(range(-5, 5))
print(s)