__author__ = 'Seryga'
x = set('abcde')
y = set('bdxyz')
a = set('zyxwv')
print(x)
print(type(x))
print(y)
print(a)
print('-'*40)
print('f' in x)  # Проверка вхождения в множество

print(x - y)  # Разность множеств
print(x | y)  # Объединение множеств
print(x & y)  # Пересечение множеств
print(x ^ y)  # Симметрическая разность (XOR)

print('-'*40)

print(x == y, x < y)  # Надмножество, подмножество
print(a)