__author__ = 'Seryga'
# описание метакласса
class myobject(type):
    # небольшое вмешательство в момент выделения памяти для класса
    def __new__(cls, name, bases, dict):
        print ("1","NEW", cls.__name__, name, bases, dict)
        return type.__new__(cls, name, bases, dict)
    # небольшое вмешательство в момент инициализации класса
    def __init__(cls, name, bases, dict):
        print ("2","INIT", cls.__name__, name, bases, dict)
        return super(myobject, cls).__init__(name, bases, dict)
# порождение класса на основе метакласса (заменяет оператор class)
MyObject = myobject("MyObject", (), {})
# обычное наследование другого класса из только что порожденного
class MySubObject(MyObject):
    def __init__(self, param):
        print (param)
# получение экземпляра класса
myobj = MySubObject('param')