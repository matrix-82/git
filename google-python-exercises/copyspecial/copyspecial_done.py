#coding: utf8
#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/


import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""
# все ниженаписанное моя реализация кроме зипа,т.к. работаю в винде

def get_special_paths(dir):
    filenames = os.listdir(dir)
    sp = []
    for file in filenames:
        pattern = re.search(r'\w+__\w+__.+', file)
        if pattern:
            sp.append(os.path.abspath(pattern.group()))
    return sp

def copy_to(paths, to_dir):
  if not os.path.exists(to_dir):
    os.mkdir(to_dir)
  for path in paths:
    shutil.copy(path, to_dir)

def zip_to(paths, zippath):
    pass


#print time.time()-t

def main():
    args = sys.argv[1:]
    if not args:
        print "usage: [--todir dir][--tozip zipfile] dir [dir ...]"
        sys.exit(1)

    elif args[0] == '--todir':
        dir = args[1]               #args[1]=sys.argv[2]
        paths = get_special_paths(args[2])
        copy_to(paths, dir)
        print('Done!')
        sys.exit(1)

    elif args[0] == '--tozip':

        pass # func zip_to(paths, zippath)

    print '\n'.join(get_special_paths(sys.argv[1]))


if __name__ == '__main__':
    main()

