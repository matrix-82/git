#coding: utf8
import  re,sys,os,urllib
#t = time.time()

def read_urls(filename):
  """Returns a list of the puzzle urls from the given log file,
  extracting the hostname from the filename itself.
  Screens out duplicate urls and returns the urls sorted into
  increasing order."""
  # +++your code here+++

def download_images(img_urls, todir):
    pass

def main():
  args = sys.argv[1:]

  if not args:
    print 'usage: [--todir dir] logfile '
    sys.exit(1)

  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  img_urls = read_urls(args[0])

  if todir:
      download_images(img_urls, todir)
  else:
      print '\n'.join(img_urls)

if __name__ == '__main__':
  main()
