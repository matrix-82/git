__author__ = 'Seryga'
x = 10
while x:
    x -= 1
    if x % 2 != 0: continue # Нечетное? – пропустить вывод
print(x, end='')

while x:
    x -= 1
    if x % 2 == 0: # Четное? - вывести
        print(x, end='')


for i in '\nhello world':
   if i == 'e':
      continue
   print(i * 2, end='')