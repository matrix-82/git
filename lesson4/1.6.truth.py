__author__ = 'Seryga'
print(2 < 3, 3 < 2)
print(2 or 3, 1 or 2)
  # Вернет левый операнд, если он имеет истинное значение
print(2 and 3, 3 and 2)
print(not 1, not 0)
print([] or 3)         # Иначе вернет правый операнд (истинный или ложный)
print([] or {})

