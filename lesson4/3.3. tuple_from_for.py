__author__ = 'Seryga'
T = [(1, 2), (3, 4), (5, 6)]
for (a,b) in T: # Операция присваивания кортежа в действии
    print(a,b)
    #print("-"*40)
print('-'*40)
for both in T:
    a, b = both
    print(a,b)

print('-'*40)


D = {'a': 1, 'b': 2, 'c': 3}
for key in D:
    print(key, '=>', D[key])    # Используется итератор словаря
                                # и операция индексирования
print('-'*40)

#list(T.items())
for (key, value) in D.items():
   print(key, '=>', value) # Обход ключей и значений одновременно

print('-'*40)