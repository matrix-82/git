__author__ = 'Seryga'
from ast import literal_eval


print('='*40)
file = open('test.txt', 'r')        # Прочитать содержимое файла в строку
print(file.read())
file = open('test.txt', 'w')
while True:
    char = file.read(1)             # Читать по одному символу
    print(file.read(1))
    if not char:
        break
    print(char)
for char in open('test.txt').read():
    print(char)
file = open('test.txt')
while True:
    line = file.readline()          # Читать строку за строкой
    if not line:
        break
    print(line, end=' ')            # Прочитанная строка уже содержит символ \n
file = open('test.txt', 'rb')
while True:
    chunk = file.read(10)           # Читать блоками по 10 байтов
    if not chunk:
        break
    print(chunk)
for line in open('test.txt').readlines():
    print(line, end='')
for line in open('test.txt'): # Использование итератора: лучший способ
    print(line, end='') # чтения текста


'''suma = 0
for x in [1, 2, 3, 4]:
    suma = x + suma
    print(file1.write(s))
    break
    file1.write(s)
print('all done')
print(s)'''


