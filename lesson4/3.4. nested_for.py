__author__ = 'Seryga'

items = ['aaa', 111, (4, 5), 2.01]     # Множество объектов
tests = [(4, 5), 3.14]                 # Ключи, которые требуется отыскать

for key in tests:                      # Для всех ключей
    for item in items:                 # Для всех элементов
        if item == key:                # Проверить совпадение
            print(key, 'was found')
            break                      # сместить на блок влево
    else:
        print(key, 'not found!')
        break


print('-'*40)

for key in tests:                      # Для всех ключей
    if key in items:                   # Позволить интерпретатору отыскать совпадение
        print(key, 'was found')
    else:
        print(key, 'not found!')

