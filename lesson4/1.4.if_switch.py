__author__ = 'Seryga'

choice = input('Enter your choice ["spam", "ham", "eggs", "bacon""]\n')
if choice == 'spam':
    print(1.25)
elif choice == 'ham':
    print (1.99)
elif choice == 'eggs':
    print (0.99)
elif choice == 'bacon':
    print (1.10)
else:
    print ('Bad choice')

print('-'*40)
branch = {'spam': 1.25,'ham': 1.99, 'eggs': 0.99}
if "spam" in branch:
    print(1.25)
else:
    print("bad choice")
if "bacon" in branch:
    print(2.20)
else:
    print("Bad choice")

print('-'*40)

print(branch.get('spam'))
print(branch.get('bacon', 'Hello!'))
